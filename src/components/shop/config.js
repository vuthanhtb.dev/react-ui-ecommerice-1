import cat1 from 'assets/images/category/cat-1.png';
import cat2 from 'assets/images/category/cat-2.png';

export const dataCategories = [
  {
    cateImg: cat1,
    cateName: "Apple",
  },
  {
    cateImg: cat2,
    cateName: "Samsung",
  },
  {
    cateImg: cat1,
    cateName: "Oppo",
  },
  {
    cateImg: cat2,
    cateName: "Vivo",
  },
  {
    cateImg: cat1,
    cateName: "Redimi",
  },
  {
    cateImg: cat2,
    cateName: "Sony",
  },
];
