import React, { useState } from 'react';

const ShopCart = (props) => {
  const { shopItems, addToCart } = props;

  const [count, setCount] = useState(0);
  const increment = () => setCount(count + 1);

  return (
    <>
      {shopItems.map((shopItems, index) => (
        <div className="box" key={index}>
          <div className="product m-top">
            <div className="img">
              <span className="discount">{shopItems.discount}% Off</span>
              <img src={shopItems.cover} alt="discount" />
              <div className="product-like">
                <label>{count}</label> <br />
                <i className="fa-regular fa-heart" onClick={increment} />
              </div>
            </div>
            <div className="product-details">
              <h3>{shopItems.name}</h3>
              <div className="rate">
                <i className="fa fa-star" />
                <i className="fa fa-star" />
                <i className="fa fa-star" />
                <i className="fa fa-star" />
                <i className="fa fa-star" />
              </div>
              <div className="price">
                <h4>{shopItems.price} VNĐ</h4>
                <button onClick={() => addToCart(shopItems)}>
                  <i className="fa fa-plus" />
                </button>
              </div>
            </div>
          </div>
        </div>
      ))}
    </>
  );
};

export default ShopCart;
