import React from 'react';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import { data } from './config';

const Cart = () => {
  const settings = {
    dots: false,
    infinite: true,
    slidesToShow: 6,
    slidesToScroll: 1,
    autoplay: true,
  };

  return (
    <Slider {...settings}>
      {data.map((value, index) => (
        <div className="box product" key={index}>
          <div className="img">
            <img src={value.cover} alt="cover" width="100%" />
          </div>
          <h4>{value.name}</h4>
          <span>{value.price}</span>
        </div>
      ))}
    </Slider>
  );
};

export default Cart;
