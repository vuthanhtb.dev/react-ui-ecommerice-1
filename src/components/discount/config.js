import discount1 from 'assets/images/discount/discount-1.png';
import discount2 from 'assets/images/discount/discount-2.png';
import discount3 from 'assets/images/discount/discount-3.png';
import discount4 from 'assets/images/discount/discount-4.png';
import discount5 from 'assets/images/discount/discount-5.png';
import discount6 from 'assets/images/discount/discount-6.png';
import discount7 from 'assets/images/discount/discount-7.png';
import discount8 from 'assets/images/discount/discount-8.png';
import discount9 from 'assets/images/discount/discount-9.png';

export const data = [
  {
    cover: discount1,
    name: "BenuX 2022",
    price: "250 000 VNĐ",
  },
  {
    cover: discount2,
    name: "Sony TV 1080p",
    price: "450 000 VNĐ",
  },
  {
    cover: discount3,
    name: "Sony PS4",
    price: "50 000 VNĐ",
  },
  {
    cover: discount4,
    name: "Setgearr 2022",
    price: "100 000 VNĐ",
  },
  {
    cover: discount5,
    name: "Tony BGB",
    price: "20 000 VNĐ",
  },
  {
    cover: discount6,
    name: "RG products",
    price: "200 000 VNĐ",
  },
  {
    cover: discount7,
    name: "Ranasonic 2022",
    price: "300 000 VNĐ",
  },
  {
    cover: discount8,
    name: "Pune HD",
    price: "30 000 VNĐ",
  },
  {
    cover: discount9,
    name: "Sony CCTV",
    price: "80 000 VNĐ",
  },
];
