import React from 'react';
import { data } from './config';

const Cart = () => {
  return (
    <div className="content grid product">
      {data.map((val, index) => (
        <div className="box" key={index}>
          <div className="img">
            <img src={val.cover} alt="cover" />
          </div>
          <h4>{val.name}</h4>
          <span>{val.price} VNĐ</span>
        </div>
      ))}
    </div>
  );
};

export default Cart;
