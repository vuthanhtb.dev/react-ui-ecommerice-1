import React from 'react';
import Navbar from './navbar';
import Search from './search';
import Head from './head';
import './index.css';

const Header = (props) => {
  const { cartItem = [] } = props;

  return (
    <>
      <Head />
      <Search cartItem={cartItem} />
      <Navbar />
    </>
  );
};

export default Header;
