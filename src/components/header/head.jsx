import React from 'react';

const Head = () => {
  return (
    <section className="head">
      <div className="container d_flex">
        <div className="left row">
          <i className="fa fa-phone" />
          <label>+84999 999 999</label>
          <i className="fa fa-envelope" />
          <label>support@ui-lib.com</label>
        </div>
        <div className="right row RText">
          <label>Theme FAQ's</label>
          <label>Need Help?</label>
          <span>🇲🇦</span>
          <label>VN</label>
          <span>🇲🇦</span>
          <label>VNĐ</label>
        </div>
      </div>
    </section>
  );
};

export default Head;
