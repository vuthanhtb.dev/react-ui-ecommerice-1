import React from 'react';
import banner1 from 'assets/images/banner-1.png';
import banner2 from 'assets/images/banner-2.png';

const Announcement = () => {
  return (
    <section className="announcement background">
      <div className="container d_flex">
        <div className="img" style={{ width: "30%", height: 340 }}>
          <img src={banner1} alt="banner1" width="100%" height="100%" />
        </div>
        <div className="img" style={{ width: "68%", height: 340 }}>
          <img src={banner2} alt="banner2" width="100%" height="100%" />
        </div>
      </div>
    </section>
  );
};

export default Announcement;
