import React from 'react';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import { data } from './config';

const TopCart = () => {
  const settings = {
    dots: false,
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    autoplay: true,
  };

  return (
    <Slider {...settings}>
      {data.map((value, index) => (
        <div className="box product" key={index}>
          <div className="name-top d_flex">
            <span className="t-left">{value.para}</span>
            <span className="t-right">{value.desc}</span>
          </div>
          <div className="img">
            <img src={value.cover} alt="cover" />
          </div>
        </div>
      ))}
    </Slider>
  );
};

export default TopCart;
