import slide1 from 'assets/images/SlideCard/slide-1.png';
import slide2 from 'assets/images/SlideCard/slide-2.png';
import slide3 from 'assets/images/SlideCard/slide-3.png';
import slide4 from 'assets/images/SlideCard/slide-4.png';

import cat1 from 'assets/images/category/cat1.png';
import cat2 from 'assets/images/category/cat2.png';
import cat3 from 'assets/images/category/cat3.png';
import cat4 from 'assets/images/category/cat4.png';
import cat5 from 'assets/images/category/cat5.png';
import cat6 from 'assets/images/category/cat6.png';
import cat7 from 'assets/images/category/cat7.png';
import cat8 from 'assets/images/category/cat8.png';
import cat9 from 'assets/images/category/cat9.png';
import cat10 from 'assets/images/category/cat10.png';
import cat11 from 'assets/images/category/cat11.png';


export const dataSlider = [
  {
    id: 1,
    title: "50% Off For Your First Shopping",
    desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quis lobortis consequat eu, quam etiam at quis ut convallis.",
    cover: slide1,
  },
  {
    id: 2,
    title: "50% Off For Your First Shopping",
    desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quis lobortis consequat eu, quam etiam at quis ut convallis.",
    cover: slide2,
  },
  {
    id: 3,
    title: "50% Off For Your First Shopping",
    desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quis lobortis consequat eu, quam etiam at quis ut convallis.",
    cover: slide3,
  },
  {
    id: 4,
    title: "50% Off For Your First Shopping",
    desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quis lobortis consequat eu, quam etiam at quis ut convallis.",
    cover: slide4,
  },
];

export const dataCategories = [
  {
    cateImg: cat1,
    cateName: "Fashion",
  },
  {
    cateImg: cat2,
    cateName: "Electronic",
  },
  {
    cateImg: cat3,
    cateName: "Cars",
  },
  {
    cateImg: cat4,
    cateName: "Home & Garden",
  },
  {
    cateImg: cat5,
    cateName: "Gifts",
  },
  {
    cateImg: cat6,
    cateName: "Music",
  },
  {
    cateImg: cat7,
    cateName: "Health & Beauty",
  },
  {
    cateImg: cat8,
    cateName: "Pets",
  },
  {
    cateImg: cat9,
    cateName: "Baby Toys",
  },
  {
    cateImg: cat10,
    cateName: "Groceries",
  },
  {
    cateImg: cat11,
    cateName: "Books",
  },
];
