import React from 'react';
import { dataCategories } from './config';

const Categories = () => {
  return (
    <div className="category">
      {dataCategories.map((value, index) => {
        return (
          <div className="box f_flex" key={index}>
            <img src={value.cateImg} alt="cateImg" />
            <span>{value.cateName}</span>
          </div>
        )
      })}
    </div>
  );
};

export default Categories;
