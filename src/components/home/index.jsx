import React from 'react';
import Categories from './categories';
import SliderHome from './sliderHome';
import './index.css';

const Home = () => {
  return (
    <section className="home">
      <div className="container d_flex">
        <Categories />
        <SliderHome />
      </div>
    </section>
  );
};

export default Home;
