import React from 'react';
import {
  AnnouncementComponent,
  DiscountComponent,
  FlashDealsComponent,
  HomeComponent,
  NewArrivalsComponent,
  ShopComponent,
  TopCateComponent,
  WrapperComponent
} from 'components';

const Product = (props) => {
  const { productItems, addToCart, shopItems } = props;

  return (
    <>
      <HomeComponent />
      <FlashDealsComponent productItems={productItems} addToCart={addToCart} />
      <TopCateComponent />
      <NewArrivalsComponent />
      <DiscountComponent />
      <ShopComponent addToCart={addToCart} shopItems={shopItems} />
      <AnnouncementComponent />
      <WrapperComponent />
    </>
  );
};

export default Product;
