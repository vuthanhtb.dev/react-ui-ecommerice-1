import React from 'react';
import { CartComponent } from 'components';

const Cart = (props) => {
  const { cartItem, addToCart, decreaseQty } = props;

  return (
    <CartComponent
      cartItem={cartItem}
      addToCart={addToCart}
      decreaseQty={decreaseQty}
    />
  );
};

export default Cart;
