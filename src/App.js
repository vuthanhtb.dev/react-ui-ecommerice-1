import React, { useState } from 'react';
import { Route, Routes } from 'react-router-dom';
import { FooterComponent, HeaderComponent } from 'components';
import { CartPage, ProductPage } from 'pages';
import DataSample from 'assets/mockData';

const App = () => {
  const { productItems, shopItems } = DataSample;

  const [cartItem, setCartItem] = useState([])

  const addToCart = (product) => {
    const productExit = cartItem.find((item) => item.id === product.id);

    if (productExit) {
      setCartItem(cartItem.map((item) => (item.id === product.id ? { ...productExit, qty: productExit.qty + 1 } : item)));
    } else {
      setCartItem([...cartItem, { ...product, qty: 1 }]);
    }
  };

  const decreaseQty = (product) => {
    const productExit = cartItem.find((item) => item.id === product.id);

    if (productExit.qty === 1) {
      setCartItem(cartItem.filter((item) => item.id !== product.id));
    } else {
      setCartItem(cartItem.map((item) => (item.id === product.id ? { ...productExit, qty: productExit.qty - 1 } : item)));
    }
  };

  return (
    <div className="app">
      <HeaderComponent cartItem={[]}/>
      <Routes>
        <Route
          path="/"
          element={
            <ProductPage
              productItems={productItems}
              addToCart={addToCart}
              shopItems={shopItems}
            />
          }
        />
        <Route
          path="/cart"
          element={
            <CartPage
              decreaseQty={decreaseQty}
              addToCart={addToCart}
              cartItem={cartItem}
            />
          }
        />
      </Routes>
      <FooterComponent />
    </div>
  );
};

export default App;
