import flash1 from 'assets/images/flash/flash-1.png';
import flash2 from 'assets/images/flash/flash-2.png';
import flash3 from 'assets/images/flash/flash-3.png';
import flash4 from 'assets/images/flash/flash-4.png';

import shop1 from 'assets/images/shops/shops-1.png';
import shop2 from 'assets/images/shops/shops-2.png';
import shop3 from 'assets/images/shops/shops-3.png';
import shop4 from 'assets/images/shops/shops-4.png';
import shop5 from 'assets/images/shops/shops-5.png';
import shop7 from 'assets/images/shops/shops-7.png';
import shop8 from 'assets/images/shops/shops-8.png';
import shop9 from 'assets/images/shops/shops-9.png';
import shop10 from 'assets/images/shops/shops-10.png';

const DataSample = {
  productItems: [
    {
      id: 1,
      discount: 50,
      cover: flash1,
      name: "Shoes",
      price: 100,
    },
    {
      id: 2,
      discount: 40,
      cover: flash2,
      name: "Watch",
      price: 20,
    },
    {
      id: 3,
      discount: 40,
      cover: flash3,
      name: "Smart Mobile Black",
      price: 200,
    },
    {
      id: 4,
      discount: 40,
      cover: flash4,
      name: "Smart Watch Black",
      price: 50,
    },
    {
      id: 5,
      discount: 50,
      cover: flash1,
      name: "Shoes",
      price: 100,
    },
    {
      id: 6,
      discount: 50,
      cover: flash3,
      name: "Shoes",
      price: 100,
    },
  ],
  shopItems: [
    {
      id: 7,
      cover: shop1,
      name: "Mapple Earphones",
      price: "180",
      discount: "25",
    },
    {
      id: 8,
      cover: shop2,
      name: "Vivo android one",
      price: "120",
      discount: "10",
    },
    {
      id: 9,
      cover: shop3,
      name: "Sony Light",
      price: "20",
      discount: "50 ",
    },
    {
      id: 10,
      cover: shop4,
      name: "Iphone",
      price: "999",
      discount: "10 ",
    },
    {
      id: 11,
      cover: shop5,
      name: "Ceats wireless earphone",
      price: "80",
      discount: "20 ",
    },
    {
      id: 12,
      cover: shop7,
      name: "Redimi Phone",
      price: "400",
      discount: "20 ",
    },
    {
      id: 13,
      cover: shop8,
      name: "Xeats Bluetooth earphones",
      price: "60",
      discount: "5 ",
    },
    {
      id: 14,
      cover: shop9,
      name: "Airpod",
      price: "120",
      discount: "10",
    },
    {
      id: 15,
      cover: shop10,
      name: "Silver Cap",
      price: "5",
      discount: "2",
    },
  ],
}

export default DataSample;
